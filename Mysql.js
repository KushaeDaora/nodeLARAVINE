

var mysql = require('mysql');
var pool  = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'testnode'
});

pool.on('release', function (connection) {
  console.log('Connection %d released', connection.threadId);
});

pool.getConnection(function(err, connection) {

	global.select = function(callback) {
		connection.query('SELECT * FROM user', function (error, results, fields) {
	    // And done with the connection.
		    //connection.release();
		    if (results != null) {
		    	if (callback) { callback(results)};
			}
			else
			{
				if (callback) { callback(error)};
			}
		})
	};


global.selectById = function(callback,id) {
		connection.query('SELECT * FROM user WHERE id='+id, function (error, userDefine, fields) {
	    // And done with the connection.
		    //connection.release();
		    if (userDefine != null) {
		    	if (callback) { callback(userDefine)};
			}
			else
			{
				if (callback) { callback(error)};
			}
		});
	};

	global.insert = function(callback,last, first, pass) {
	  	connection.query('INSERT INTO user SET ?', {'last': last, 'first':first , 'pass': pass}, function (error, results, fields) {
	    // And done with the connection.
	    //connection.release();
	    if (results != null) {
	    	if (callback) { callback(results)};
		}
		else
		{
			if (callback) { callback(error)};
		}

	})
};

	global.update = function(callback, last, first, pass, id) {
	connection.query('UPDATE user SET ? WHERE id='+id, {'last': last, 'first':first, 'pass':pass}, function (error, results, fields) {
	    // And done with the connection.

	    if (results != null) {
	    	if (callback) { callback(results)};
		}
		else
		{
			if (callback) { callback(error)};
		}

	})
};

	global.deletes = function(callback,id) {
		connection.query('DELETE FROM user WHERE id=' + id, function (error, results, fields) {
	    // And done with the connection.
		    //connection.release();
		    if (results != null) {
		    	if (callback) { callback(results)};
			}
			else
			{
				if (callback) { callback(error)};
			}
		})
	};



});

