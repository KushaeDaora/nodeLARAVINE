$(document).ready(function () {
	$('button[data-action="remove"]').on('click', function() {
		var id  = $(this).closest('li').data('id');
		$.ajax({
			'url' : '/user/remove',
			'method' : 'POST',
			'data': {'id':id},
			'success': function(json) {
				
					var url = window.location.href;
					window.location.href = url;
				
			}
		});
	});

	$('button[data-action="update"]').on('click', function() {
		var id  = $(this).closest('li').data('id');
		window.location.href = "/user/update/"+id;
	});
});