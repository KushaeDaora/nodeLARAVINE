global.express = require('express');
global.app = express();

global.bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: false}));
var mysql = require('./Mysql');

app.get('/users', function(req,res) {
	select(function(results) { 
		res.render('ListUsersForm.ejs', {results:results});
		res.end();
	});


});

app.post('/user/remove', function(req,res) {
	deletes(function() {},req.body.id);
	res.end();
});

app.post('/addUserForm', function(req,res) {
	res.render('addUser.ejs');
});

app.get('/user/update/:id', function(req,res) {
	selectById(function(userDefine) {
		res.render('updateUser.ejs',{userDefine:userDefine});
		console.log(userDefine);
		res.end();
	}, req.params.id);
	
	//
	
});

app.post('/updateUser/:id', function(req,res) {
	update(function() {}, req.body.last, req.body.first, req.body.pass, req.params.id);
	res.redirect('/users');
});

app.post('/addUser', function(req,res) {

	insert(function() {}, req.body.last, req.body.first, req.body.pass );
	res.redirect('/users');
})

app.get('/jquery.min.js', function(req,res) {
	res.writeHead(200, {'Content-Type': 'text/javascript; charset=utf-8'});
	res.write(require('fs').readFileSync('/workspace/node_modules/jquery/dist/jquery.min.js', 'utf-8'));
	res.end();
});

app.get('/remove.js', function(req,res) {
	res.writeHead(200, {'Content-Type': 'text/javascript; charset=utf-8'});
	res.write(require('fs').readFileSync('/workspace/CoursNode/user/views/js/remove.js', 'utf-8'));
	res.end();
});

app.listen(8080);